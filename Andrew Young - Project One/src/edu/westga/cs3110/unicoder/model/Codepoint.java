package edu.westga.cs3110.unicoder.model;

/**
 * @author andrew young
 * 
 * Class to encode between hexadecimal strings and codepoints
 */
public class Codepoint {

	private String codepointAsHexadecimalString;
	
	/**
	 * Constructs a Codepoint object to perform various UTF conversions.
	 * 
	 * @param hexadecimalString the hexadecimal string to convert
	 * 
	 * @precondition hexadecimalString cannot be empty or null
	 * @postcondition none
	 */
	public Codepoint(String hexadecimalString) {
		if (hexadecimalString == null || hexadecimalString.isEmpty()) {
			throw new IllegalArgumentException("Invalid Parameter - Parameter can't be null or empty");
		}
		if (hexadecimalString.toLowerCase().contains("u+")) {
			throw new IllegalArgumentException("Invalid Parameter - Parameter can't contain 'U+'");
		}
		this.codepointAsHexadecimalString = hexadecimalString;
	}
	
	/**
	 * Returns a string representation of the object
	 * @return a string representation of the object
	 */
	public String toString() {
		return this.codepointAsHexadecimalString;
	}
	
	/**
	 * Returns UTF32 representation of the hexademicimal string
	 * @return UTF32 representation of the hexademicimal string
	 */
	public String toUTF32() {
		return String.format("%08X", this.getCodepointAsInteger());
	}
	
	/**
	 * Returns the codepoint in UTF-16 encoding, as either a 4-digit or 8-digit hexadecimal string
	 * @return the codepoint in UTF-16 encoding, as either a 4-digit or 8-digit hexadecimal string
	 */
	public String toUTF16() {
		int codepoint = this.getCodepointAsInteger();
		if (codepoint >= Constants.UTF16_FOUR_BYTE_LOWERBOUND && codepoint <= Constants.UTF16_FOUR_BYTE_UPPERBOUND) {
			int p = codepoint - Constants.UTF16_FOUR_BYTE_LOWERBOUND;
					
			int highSurrogate = (p >>> 10) + 0xD800;
			int lowSurrogate = (p & Constants.LOWER_TEN_BIT_MASK) + 0xDC00;
			
			String highSurrogateToString = Integer.toHexString(highSurrogate);
			String lowSurrogateToString = Integer.toHexString(lowSurrogate);
			
			return (highSurrogateToString + lowSurrogateToString).toUpperCase();
		} else if (codepoint >= 0X0000 && codepoint <= 0XD7FF){
			return String.format("%04X", codepoint).toUpperCase();
		}

		return "INVALID CODEPOINT FOR UTF16";
	}
	
	/**
	 * Returns the codepoint in UTF-8 encoding, as either a 2-digit, 4-digit, 6-digit, or 8-digit hexadecimal string
	 * @return The codepoint in UTF-8 encoding, as either a 2-digit, 4-digit, 6-digit, or 8-digit hexadecimal string
	 */
	public String toUTF8() {
		int codepoint = this.getCodepointAsInteger();

		if (codepoint >= 0X0000 && codepoint <= 0X007F) {
			return getSingleByteUTF8Encoding(codepoint);
		} else if (codepoint >= 0X0080 && codepoint <= 0X07FF) {
			return getTwoByteUTF8Encoding(codepoint);
		} else if (codepoint >= 0X0800 && codepoint <= 0XFFFF) {
			return getThreeByteUTF8Encoding(codepoint);
		} else if (codepoint >= 0X10000 & codepoint <= 0X10FFFF) {
			return getFourByteUTF8Encoding(codepoint);
		}
		
		return "INVALID CODEPOINT FOR UTF8";
	}
	
	private String getSingleByteUTF8Encoding(int codepoint) {
		int result = codepoint & 0X000000FF;
		return String.format("%02X", result);
	}
	
	private String getTwoByteUTF8Encoding(int codepoint) {
		int upperFiveBits = (codepoint >>> 6) & 0XDF;
		int lowerSixBits  = (codepoint & 0X0003F);
		
		int byteOne = 0XC0 | upperFiveBits;
		int byteTwo = 0X80 | lowerSixBits;
		
		String byteOneAsString = Integer.toHexString(byteOne);
		String byteTwoAsString = Integer.toHexString(byteTwo);
		
		String result = (byteOneAsString + byteTwoAsString).toUpperCase();
		return result;
	}
	
	private String getThreeByteUTF8Encoding(int codepoint) {
		int upperFourBits = (codepoint >>> 12) & 0XEF;
		int nextSixBits = (codepoint >>> 6) & 0X0BF;
		int finalThreeBits = (codepoint) & 0X00BF;
		
		int byteOne = 0XE0 | upperFourBits;
		int byteTwo = 0X80 | nextSixBits;
		int byteThree = 0X80 | finalThreeBits;
		
		String byteOneString = Integer.toHexString(byteOne);
		String byteTwoString = Integer.toHexString(byteTwo);
		String byteThreeString = Integer.toHexString(byteThree);
		
		return (byteOneString + byteTwoString + byteThreeString).toUpperCase();
	}
	
	private String getFourByteUTF8Encoding(int codepoint) {
		int upperFourBits = (codepoint >>> 18) & 0XF7;
		int nextSixBits = (codepoint >>> 12) & 0X000BF;
		int nextNextSixBits = (codepoint >>> 6) & 0X000BF;
		int finalSixBits = (codepoint) & 0X000BF;
		
		int byteOne = 0XF0 | upperFourBits;
		int byteTwo = 0X80 | nextSixBits;
		int byteThree = 0X80 | nextNextSixBits;
		int byteFour = 0X80 | finalSixBits;
		
		String byteOneString = Integer.toHexString(byteOne);
		String byteTwoString = Integer.toHexString(byteTwo);
		String byteThreeString = Integer.toHexString(byteThree);
		String byteFourString = Integer.toHexString(byteFour);
	
		String result = byteOneString + byteTwoString + byteThreeString + byteFourString;
		return result.toUpperCase();
	}
	
	private int getCodepointAsInteger() {
		return Integer.parseUnsignedInt(this.codepointAsHexadecimalString, 16);
	}
}
