package edu.westga.cs3110.unicoder.model;

/**
 * Commonly used constants for the Codepoint class
 */
public class Constants {
	public static final int UTF16_FOUR_BYTE_LOWERBOUND = 0x10000;
	public static final int UTF16_FOUR_BYTE_UPPERBOUND = 0x10FFFF;
	public static final int LOWER_TEN_BIT_MASK = 0x000003FF;
}
