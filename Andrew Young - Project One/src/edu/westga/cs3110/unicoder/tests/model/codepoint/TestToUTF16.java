package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

class TestToUTF16 {
	
	@Test
	void testToUTF16AsTwoBytes() {
		String hexadecimalString = "ABCD";
		String expected = "ABCD";
		Codepoint codepoint = new Codepoint(hexadecimalString);
		String result = codepoint.toUTF16();
		
		assertEquals(expected, result);
	}

	@Test
	void testToUTF16AsFourBytes() {
		Codepoint codepoint = new Codepoint("0183A5");
		String expected = "D820DFA5";
		String result = codepoint.toUTF16();
		
		assertEquals(expected, result);
	}
	
	@Test
	void testToUTF16TwoByteOutOfBounds() {
		Codepoint codepoint = new Codepoint("D8FF");
		String expected = "INVALID CODEPOINT FOR UTF16";
		String result = codepoint.toUTF16();
		
		assertEquals(expected, result);
	}

}
