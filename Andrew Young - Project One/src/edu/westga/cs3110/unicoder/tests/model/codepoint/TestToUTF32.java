package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

class TestToUTF32 {
	
	@Test
	void testToUTF32() {
		String hexadecimalString = "000183A5";
		String expected = "000183A5";
		Codepoint codepoint = new Codepoint(hexadecimalString);
		String result = codepoint.toUTF32();
		
		assertEquals(expected, result);
	}
}
