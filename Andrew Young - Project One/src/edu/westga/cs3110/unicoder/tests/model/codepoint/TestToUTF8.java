package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

class TestToUTF8 {

	@Test
	void testToUTF8AsSingleByte() {
		String hexadecimalString = "0015";
		String expected = "15";
		Codepoint codepoint = new Codepoint(hexadecimalString);
		String result = codepoint.toUTF8();
		
		assertEquals(expected, result);
	}
	
	@Test
	void testToUTF8AsTwoBytes() {
		String hexadecimalString = "01A0";
		String expected = "C6A0";
		Codepoint codepoint = new Codepoint(hexadecimalString);
		String result = codepoint.toUTF8();
		
		assertEquals(expected, result);
	}
	
	@Test
	void testToUTF8AsThreeBytes() {
		String hexadecimalString = "4CE3";
		String expected = "E4B3A3";
		Codepoint codepoint = new Codepoint(hexadecimalString);
		String result = codepoint.toUTF8();
		
		assertEquals(expected, result);
	}
	
	@Test
	void testToUTF8AsFourBytes() {
		String hexadecimalString = "10FFEF";
		String expected = "F48FBFAF";
		Codepoint codepoint = new Codepoint(hexadecimalString);
		String result = codepoint.toUTF8();
		
		assertEquals(expected, result);
	}
	
	@Test
	void testInvalidUTF8() {
		String hexadecimalString = "11FFFF";
		String expected = "INVALID CODEPOINT FOR UTF8";
		Codepoint codepoint = new Codepoint(hexadecimalString);
		String result = codepoint.toUTF8();
		
		assertEquals(expected, result);
	}

}
