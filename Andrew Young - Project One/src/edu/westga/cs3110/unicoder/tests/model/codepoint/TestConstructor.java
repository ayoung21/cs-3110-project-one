package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

class TestConstructor {

	@Test
	void testNullParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Codepoint(null);
		});
	}
	
	@Test
	void testEmptyParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Codepoint(null);
		});
	}
	
	@Test
	void testPrefixedParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Codepoint("U+10FFFF");
		});
	}
	
	@Test
	void testValidParameter() {
		Codepoint codepoint = new Codepoint("000183A5");
		String expected = "000183A5";
		String result   = codepoint.toString();
		
		assertEquals(expected, result);
	}

}
